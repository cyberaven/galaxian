﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Fleet : MonoBehaviour
{
    [SerializeField] Ship _shipPrefab;
    [SerializeField] List<Ship> shipList = new List<Ship>();
    List<int> fleetMap = new List<int>();

    int rowCount = 3;
    int columnCount = 5;

    float upDownSpeed = -1;

    [SerializeField] bool move = false;

    private void Start()
    {
        Subscribe();
    }    
    void Subscribe()
    {
        LevelStartScreen.LevelStartScreenEvent += MoveEnable;
    }      

    public void Create(List<int> setFleetMap)
    {
        fleetMap = setFleetMap;
        Fill();
        SetShipPosition();
        SetFleetPosition();
        Rotate();
        MoveOnPosition();
    }
    
    void Fill()
    {
        for (int i = 0; i < fleetMap.Count; i++)
        {
            Ship ship = CreateShip(fleetMap[i]);
            shipList.Add(ship);
        }
    }
    Ship CreateShip(int spriteId)
    {
        Ship ship = Instantiate(_shipPrefab, transform);
        ship.SetSprite(spriteId);
        return ship;
    }
    void SetShipPosition()
    {
        int a = 0;
        for (int y = 0; y < rowCount; y++)
        {
            for (int x = 0; x < columnCount; x++)
            {
                shipList[a].transform.localPosition = new Vector3(x, y, 0);                
                a++;
            }
        }
    }
    void SetFleetPosition()
    {
        transform.localPosition = new Vector3(-2f,-7.5f,0f);
    }
    void Rotate()
    {
        transform.RotateAround(Vector3.zero, Vector3.forward, 180f);
    }
    private void MoveOnPosition()
    {
        StartCoroutine(MoveDown());                
    }


    IEnumerator MoveDown()
    {
        while (true)
        {
            transform.Translate(Vector3.down * Time.deltaTime * upDownSpeed);

            if (transform.localPosition.y < 4)
            {               
                StopAllCoroutines();
                StartCoroutine(MovePingPong());
            }
            yield return null;
        }
    }

    IEnumerator MovePingPong()
    {
        Vector3 direction = Vector3.right;
        while(true)
        {
            if (move == true)
            {
                if (transform.localPosition.x < 1)
                {
                    direction = Vector3.right;                    
                }
                else if(transform.localPosition.x > 3)
                {
                    direction = Vector3.left;                   
                } 
                transform.Translate(direction * Time.deltaTime * upDownSpeed);
            }            
            yield return null;
        }        
    }

    void MoveEnable()
    {
        if (move == false)
        {
            move = true;
        }
        else
        {
            move = false;
        }
    }
       
    public void CheckShipInFleet()
    {
        int a = 0;
        foreach (var ship in shipList)
        {
            if(ship == null)
            {                
                a++;
                int i = fleetMap.Count;
                
                if (i - a <= 1)
                {
                    Game.instance.NoShipInFleet();                    
                }
            }
        }    
    }
    public void PlayerDie()
    {
        foreach (var ship in shipList)
        {
            ship.ShootEnable();
        }
    }
}