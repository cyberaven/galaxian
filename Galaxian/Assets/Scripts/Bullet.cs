﻿using UnityEngine;

public class Bullet : MonoBehaviour
{
    public Ship owner;    
   
    public void Owner(Ship ship)
    {
        owner = ship;
    }

    private void Update()
    {
        FlyAway();
        CheckDistaneceToDie();
    }

    void FlyAway()
    {
        if (owner.isPlayer == true)
        {
            transform.Translate(Vector3.up * Time.deltaTime);
        }
        else
        {
            transform.Translate(Vector3.down * Time.deltaTime);
        }
    }
    void CheckDistaneceToDie()
    {
        if(transform.position.y < -7 || transform.position.y > 5)
        {
            Destroy(this.gameObject);
        }
    }


}
