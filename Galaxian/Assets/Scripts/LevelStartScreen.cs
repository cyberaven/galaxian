﻿using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

public class LevelStartScreen : GamePanel
{
    [SerializeField] Text text;

    public delegate void LevelStartScreenCloseDel();
    public static event LevelStartScreenCloseDel LevelStartScreenEvent;

    private void OnDisable()
    {
        if (LevelStartScreenEvent != null)
        {
            LevelStartScreenEvent();
        }
    }   
}