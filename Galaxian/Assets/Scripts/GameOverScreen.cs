﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverScreen : GamePanel
{
    [SerializeField] private Button mainMenuBtn;

    public delegate void GameOverScreenCloseDel();
    public static event GameOverScreenCloseDel GameOverScreenEvent;

    private void Start()
    {
        mainMenuBtn.onClick.AddListener(MainMenuBtnClk);

        if(GameOverScreenEvent != null)
        {
            GameOverScreenEvent();
        }
    }
    
    void MainMenuBtnClk()
    {
        Game.instance.DestroyLevel();        
        Game.instance.MainMenuBtnEvent();        
    }
}
