﻿using UnityEngine;
using System.Collections;

public class GamePanel : MonoBehaviour
{    
    float showTime;

    public void Show()
    {
        gameObject.SetActive(true);
    }    
    public void Show(float time)
    {       
        showTime = time;
        gameObject.SetActive(true);
    }
    public void Hide()
    {
        gameObject.SetActive(false);       
    }

    private void Update()
    {
        if(showTime>0)
        {
            showTime -= Time.deltaTime;
           
            if (showTime <= 0)
            {
                Hide();
            }
        }        
    }
}
