﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : GamePanel
{
    [SerializeField] Button startBtn;
    [SerializeField] Button randomLevelBtn;    
   
    private void Start()
    {
        startBtn.onClick.AddListener(StartBtnClk);
        randomLevelBtn.onClick.AddListener(RandomLevelBtnClk);
    }

    void StartBtnClk()
    {
        Game.instance.StartGame();        
    }

    void RandomLevelBtnClk()
    {
        Game.instance.LoadRandomLevel();
    }
    
}
