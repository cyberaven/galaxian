﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelHandler : MonoBehaviour
{   
    [SerializeField] Level _levelPrefab;//prefab    
    Level currentLevel;
    int currentLevelNumb;

    bool rand = false;

    List<List<int>> fleetMaps = new List<List<int>>()//маска флота по id спрайта.
    {
        new List<int>{1,2,3,2,1,2,1,2,1,2,1,1,1,1,1},//15 шт. 3 ряда по 5
        new List<int>{3,2,3,2,3,2,1,3,1,2,1,1,3,1,1},
        new List<int>{2,2,1,2,2,1,1,3,1,1,1,2,1,2,1}
    };

    public void NewGame()
    {
        rand = false;
        currentLevelNumb = 0;
        NewLevel(currentLevelNumb);
    }    
    public void RandLevel()
    {
        rand = true;
        int i = Random.Range(0, fleetMaps.Count);
        NewLevel(i);        
    }

    void NewLevel(int levelNumb)
    {
        DestroyCurrentLevel();
        currentLevel = Instantiate(_levelPrefab);
        currentLevel.Load(fleetMaps[levelNumb]);
    }
    public void NextLevel()
    {
        if (rand == false)
        {
            currentLevelNumb += 1;
            if (currentLevelNumb < fleetMaps.Count)
            {
                Debug.Log("Start level: "+currentLevelNumb);
                NewLevel(currentLevelNumb);
            }
            else
            {
                Game.instance.ShowWinScreen();
            }
        }
        else
        {
            DestroyCurrentLevel();
            Game.instance.ShowWinScreen();
        }
    }

    
    public void DestroyCurrentLevel()
    {
        if(currentLevel != null)
        {
            Destroy(currentLevel.gameObject);
        }
    }   
    public void PlayerDie()
    {
        currentLevel.PlayerDie();
    }
}
