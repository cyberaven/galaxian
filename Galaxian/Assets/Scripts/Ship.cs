﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour
{
    public bool isPlayer = false;
    public bool shootEnable = false;

    float shootTime = 1;
    float time;

    public int hp = 10;

    [SerializeField] Bullet _bulletPrefab;
    [SerializeField] List<Sprite> shipSprite = new List<Sprite>();

    public delegate void PlayerHitDel(Ship ship);
    public static event PlayerHitDel PlayerHitEve;

    private void Start()
    {
        Subscrabe();

        SetupPlrOrNPC();        
    }
    private void Update()
    {
        if (shootEnable == true)
        {
            Shoot();
        }
    }

    void Subscrabe()
    {
        LevelStartScreen.LevelStartScreenEvent += ShootEnable;        
    }
    void SetupPlrOrNPC()
    {
        if (isPlayer == true)
        {
            SetStartPosition();
            SetSprite(0);
        }
        else
        {
            RandomShootTime();
            RandomHP();
        }
    }

    void SetStartPosition()
    {
        transform.localPosition = new Vector3(0f, -4f, 0f);
    }

    public void SetSprite(int spriteId)
    {
        GetComponent<SpriteRenderer>().sprite = shipSprite[spriteId];
    }
    
    public void ShootEnable()
    {
        if(shootEnable == false)
        {
            shootEnable = true;
        }
        else
        {
            shootEnable = false;
        }
        
    }
    void Shoot()
    {
        time += Time.deltaTime;

        if(isPlayer == true)
        {
            shootTime = 0.5f;///// костыль чтобы ускорить тест игры.
        }

        if (time > shootTime)
        {
            Bullet bullet = Instantiate(_bulletPrefab);
            bullet.transform.position = transform.position;
            bullet.Owner(this);
            time = 0;
        }
            
    }
    void RandomShootTime()
    {
        shootTime = Random.Range(1, 10);
    }
    void RandomHP()
    {
        hp = Random.Range(1,5);
    }

    void OnMouseDrag()
    {
        if (isPlayer == true)
        {
            Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, -1 * (Camera.main.transform.position.z));
            Vector3 objPosition = Camera.main.ScreenToWorldPoint(mousePosition);
            transform.position = objPosition;
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.tag == "Bullet")
        {
            Bullet bullet = col.gameObject.GetComponent<Bullet>();

            if(bullet.owner.isPlayer != isPlayer)
            {
                Hit();
                Destroy(bullet.gameObject);
            }
        }
    }

    void Hit()
    {
        hp--;

        if(isPlayer == true)
        {
            if(PlayerHitEve != null)
            {
                PlayerHitEve(this);
            }
        }

        if(hp <= 0)
        {
            Die();
        }
    }
    void Die()
    {
        if (isPlayer == false)
        {
            Fleet fleet = GetComponentInParent<Fleet>();
            fleet.CheckShipInFleet();
        }

        if (isPlayer == true)
        {
            Game.instance.PlayerDie();
        }

        Destroy(this.gameObject);        
    }
}