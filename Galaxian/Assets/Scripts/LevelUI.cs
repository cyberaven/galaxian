﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LevelUI : GamePanel
{
    [SerializeField] Text scoreTxt;
    [SerializeField] Text hpTxt;
   
    private void Start()
    {
        Subscrabe();             
    }

    void Subscrabe()
    {
        Ship.PlayerHitEve += ShowHPTxt;
    }

    void ShowHPTxt(Ship ship)
    {
        hpTxt.text = "HP: "+ ship.hp +".";
    }
   
}
