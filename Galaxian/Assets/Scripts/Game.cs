﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{

    [SerializeField] private GameUI _gameUIPrefab;//prefabs
    private GameUI gameUI;

    [SerializeField] private LevelHandler _levelHandlerPrefab;
    private LevelHandler levelHandler;

    public static Game instance;
    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }


    private void Start()
    {
        InitGame();        
    }
    void InitGame()
    {
        InitUI();
        InitLevelHandler();
    }
    void InitUI()
    {
        gameUI = Instantiate(_gameUIPrefab);
    }
    void InitLevelHandler()
    {
        levelHandler = Instantiate(_levelHandlerPrefab);
    }
    

    public void StartGame()
    {
        gameUI.LoadLevelUI();
        levelHandler.NewGame();
    }
    void NextLevel()
    {        
        gameUI.LoadLevelUI();
        levelHandler.NextLevel();
    }
    public void LoadRandomLevel()
    {
        gameUI.LoadLevelUI();
        levelHandler.RandLevel();
    }

    public void PlayerDie()
    {
        gameUI.PlayerDie();
        levelHandler.PlayerDie();
    }
    public void NoShipInFleet()
    {
        NextLevel();
    }

    public void DestroyLevel()
    {
        levelHandler.DestroyCurrentLevel();       
    }    
    public void ShowLevelStartScreen()
    {
        gameUI.ShowLevelStartScreen();
    }
    public void ShowMainMenu()
    {
        gameUI.ShowMainMenu();
    }
    public void ShowWinScreen()
    {
        DestroyLevel();
        gameUI.ShowWinScreen();        
    }    
    public void MainMenuBtnEvent()
    {
        gameUI.MainMenuBtnEvent();
    }    
}
