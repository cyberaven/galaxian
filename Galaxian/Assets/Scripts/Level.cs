﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{    
    [SerializeField] Ship _shipPrefab;
    List<Ship> currentShips = new List<Ship>();

    [SerializeField] Fleet _fleetPrefab;    
    Fleet fleet;    

    private void Start()
    {
        ShowLevelStartScreen();
    }    
    
    public void Load(List<int> fleet)
    {      
        CreateFleet(fleet);
        CreatePlayer();
        ShowLevelStartScreen();
    }     

    void CreateFleet(List<int> fleetMap)
    {
        fleet = Instantiate(_fleetPrefab, transform);
        fleet.Create(fleetMap);        
    }
    void CreatePlayer()
    {
        Ship player = Instantiate(_shipPrefab, transform);
        player.isPlayer = true;                
    }

    void ShowLevelStartScreen()
    {
        Game.instance.ShowLevelStartScreen();        
    }

    public void PlayerDie()
    {
        fleet.PlayerDie();
    }



}