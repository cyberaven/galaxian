﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameUI : MonoBehaviour
{
    [SerializeField] private MainMenu _mainMenuPrefab;//prefab
    private MainMenu mainMenu;

    [SerializeField] private LevelUI _levelUIPrefab;//prefab
    private LevelUI levelUI;

    [SerializeField] private LevelStartScreen _levelStartScreenPrefab;//prefab
    private LevelStartScreen levelStartScreen;

    [SerializeField] private GameOverScreen _gameOverScreenPrefab;
    private GameOverScreen gameOverScreen;

    [SerializeField] private WinScreen _winScreenPrefab;
    private WinScreen winScreen;    

    private void Start()
    {
        InitUI();       

        HideAll();
        ShowMainMenu();
    }

    void InitUI()
    {
        mainMenu = Instantiate(_mainMenuPrefab, transform);
        levelUI = Instantiate(_levelUIPrefab, transform);
        levelStartScreen = Instantiate(_levelStartScreenPrefab, transform);
        gameOverScreen = Instantiate(_gameOverScreenPrefab, transform);
        winScreen = Instantiate(_winScreenPrefab, transform);
    }  

    public void ShowLevelStartScreen()
    {
        levelStartScreen.GetComponent<GamePanel>().Show(3f);
    }
    public void PlayerDie()
    {
        levelUI.Hide();
        gameOverScreen.Show();
    }
    public void MainMenuBtnEvent()
    {
        gameOverScreen.Hide();
        mainMenu.Show();
    }

    public void ShowMainMenu()
    {
       HideAll();
       mainMenu.Show();
    }
    public void HideMainMenu()
    {
       mainMenu.Hide();
    }    
    public void LoadLevelUI()
    {
        HideAll();
        levelUI.Show();
    }
    public void ShowWinScreen()
    {
        HideAll();
        winScreen.GetComponent<GamePanel>().Show(5f);
    }



    public void HideAll()
    {
        var allGamePanel = GetComponentsInChildren<GamePanel>();

        foreach (var gamePanel in allGamePanel)
        {
            gamePanel.Hide();
        }
    }

}
